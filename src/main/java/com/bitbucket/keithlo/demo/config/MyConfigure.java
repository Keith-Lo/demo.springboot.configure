package com.bitbucket.keithlo.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="hello")
public class MyConfigure {
    
    private String world;

    private String winDir;

    private String macDir;


    public String getWorld(){
        return world;
    }

    public void setWorld(String world){
        this.world = world;
    }

    public String getWinDir(){
        return winDir;
    }

    public void setWinDir(String winDir){
        this.winDir = winDir;
    }

    public String getMacDir(){
        return macDir;
    }

    public void setMacDir(String macDir){
        this.macDir = macDir;
    }
}
