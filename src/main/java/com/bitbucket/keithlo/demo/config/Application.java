package com.bitbucket.keithlo.demo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
public class Application implements CommandLineRunner{

	private static Logger log = LoggerFactory.getLogger(Application.class);

	@Autowired
	MyConfigure myConfigure;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);		
	}

	@Override
    public void run(String... args) {
        log.info("Application Started.");
 
        log.info("World: {}", myConfigure.getWorld());
        log.info("Win Dir: {}", myConfigure.getWinDir());
        log.info("Mac Dir: {}", myConfigure.getMacDir());
    }

}
